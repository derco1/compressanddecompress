import zlib from "zlib";

async function compress(txt: string|object): Promise<string> {
    let textContent = typeof txt === 'object' ? JSON.stringify(txt) : txt;
    return new Promise((resolve, reject) => {
        const buffer = Buffer.from(textContent, 'utf-8');
        zlib.deflate(buffer, (err: any, compressedBuffer: any) => {
            if (err) {
                console.error('Error al comprimir:', err);
                reject(err);
            }

            console.log(`String original: ${buffer.length} bytes`);

            const base64Compressed = compressedBuffer.toString('base64');
            console.log(`String comprimido: ${base64Compressed.length} bytes`);
            console.log(`Porcentaje de compresión: ${((1 - base64Compressed.length / textContent.length) * 100).toFixed(2)}%`);
            resolve(base64Compressed);

            //*********************************************************** */
            //Recordar eliminar logs al copiar este codigo a los servicios
            //*********************************************************** */
        });
    });
}

async function decompress(base64Compressed: string): Promise<string> {
    return new Promise((resolve, reject) => {
        const compressedBuffer = Buffer.from(base64Compressed, 'base64');
        zlib.inflate(compressedBuffer, (err: any, decompressedBuffer: any) => {
            if (err) {
                console.error('Error al descomprimir:', err);
                reject(err);
            }

            const decompressedString = decompressedBuffer.toString('utf-8');
            resolve(decompressedString);
        });
    });
}

export { compress, decompress };