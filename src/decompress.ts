import { decompress } from "./functions";

const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question('enter compress content: ', async (input: string) => {
    const decompressedString = await decompress(input);
    console.log('Decompress content:', decompressedString);
    rl.close();
});